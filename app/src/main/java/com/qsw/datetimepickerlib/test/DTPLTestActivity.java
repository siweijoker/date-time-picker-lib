package com.qsw.datetimepickerlib.test;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.qsw.datetimepickerlib.DateTimePicker.view.DateTimePicker;
import com.qsw.datetimepickerlib.R;

import java.util.Calendar;

public class DTPLTestActivity extends Activity {

    private DateTimePicker mDTP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dtpl_activity_test);

        mDTP = findViewById(R.id.dtpl_dtp);

        Calendar minCalendar = Calendar.getInstance();
        minCalendar.set(2022, 2, 2, 2, 2);
        mDTP.setMinDate(minCalendar.getTime());
        mDTP.setShowFutureDate(false);
        mDTP.setOnDateTimeChangedListener(new DateTimePicker.OnDateTimeChangedListener() {
            @Override
            public void onChanged(int year, int month, int day, int hour, int minute) {
                Log.d("TestDTPL", year + " : " + month + " : " + day + " : " + hour + " : " + minute);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDTP.setDateTime(2022, 6, 6, 2, 6);
    }
}
