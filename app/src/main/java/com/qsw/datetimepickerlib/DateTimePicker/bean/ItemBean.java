package com.qsw.datetimepickerlib.view.DateTimePicker.bean;

/**
 * WheelView中条目实例
 */
public class ItemBean {

    private String str;
    private float x;
    private float y;
    private int alpha;
    private float scale;

    public ItemBean(String str, float x, float y) {
        this(str, x, y, 0, 1);
    }

    public ItemBean(String str, float x, float y, int alpha, float scale) {
        this.str = str;
        this.x = x;
        this.y = y;
        this.alpha = alpha;
        this.scale = scale;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }
}
